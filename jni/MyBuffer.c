#include "jni.h"
#include <stdio.h>
#include "cn_leafw_pmdk_example_my_MyBuffer.h"

JNIEXPORT void JNICALL Java_cn_leafw_pmdk_example_my_MyBuffer_getBuffer
  (JNIEnv * env, jobject jobj, jlong address){

printf("in = %ld\n", address);
  jlong *p;
  p = &address;
  printf("p = %p\n", p);

  jobject jbuffer = (*env)->NewDirectByteBuffer(env, p, 4);

//  jlong a = (*env)->GetDirectBufferCapacity(env, jbuffer);
//  printf("cap = %ld\n", a);

  void* body = (*env)->GetDirectBufferAddress(env, jbuffer);
  printf("address = %p\n", body);
   return ;
  }