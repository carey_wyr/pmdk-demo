package cn.leafw.pmdk.example.mq;

import cn.leafw.pmdk.example.constant.Constant;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import static cn.leafw.pmdk.example.constant.Constant.*;

/**
 * TODO
 *
 * @author <a href="mailto:wyr95626@95626.cn">CareyWYR</a>
 * @date 2022/4/18
 */
public class WriteMsg {
    public static final String MMAP_PATH = "/Users/carey/Documents/paper/temp/bb";
    public static final String HEAP_PATH = "/Users/carey/Documents/paper/temp/bb";
    public static final String POOL_PATH = "/Users/carey/Documents/paper/temp/bb";

    /**
     * init time: 1377
     * fileChannel write time: 1257
     * force time: 3, total time: 1261
     * @param args
     */
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        TransientStorePool transientStorePool = new TransientStorePool(2, GB.intValue());
        transientStorePool.init();
        long time1 = System.currentTimeMillis();
        System.out.printf("init time: %s%n", time1 - start);
        ByteBuffer byteBuffer = transientStorePool.borrowBuffer();
        File file = new File(MMAP_PATH);
        try {
            FileChannel fileChannel =  new RandomAccessFile(file, "rw").getChannel();
            for (int i = 0; i < 4 * KB; i++) {
                byteBuffer.put((byte)0);
            }
            for (int i = 0; i < GB; i += 4*KB) {
                byteBuffer.position(0);
                byteBuffer.limit(4*KB.intValue());
                fileChannel.write(byteBuffer);
            }
            long time2 = System.currentTimeMillis();
            System.out.printf("fileChannel write time: %s%n", time2 - time1);
            fileChannel.force(false);
            System.out.printf("force time: %s, total time: %s%n", System.currentTimeMillis() - time2, System.currentTimeMillis() - time1);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

