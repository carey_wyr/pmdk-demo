package cn.leafw.pmdk.example.constant;

/**
 * Constant
 *
 * @author <a href="mailto:wyr95626@95626.cn">CareyWYR</a>
 * @date 2022/4/18
 */
public class Constant {

    public static final Long KB = 1024L;
    public static final Long MB = 1024L * 1024;
    public static final Long GB = 1024L * 1024 * 1024;
}

