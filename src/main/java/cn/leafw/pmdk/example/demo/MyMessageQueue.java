package cn.leafw.pmdk.example.demo;

import java.nio.ByteBuffer;
import java.util.Map;

/**
 * TODO
 *
 * @author <a href="mailto:wyr95626@95626.cn">CareyWYR</a>
 * @date 2022/7/4
 */
public class MyMessageQueue extends MessageQueue{

    @Override
    public long append(String topic, int queueId, ByteBuffer data) {
        return 0;
    }

    @Override
    public Map<Integer, ByteBuffer> getRange(String topic, int queueId, long offset, int fetchNum) {
        return null;
    }
}

