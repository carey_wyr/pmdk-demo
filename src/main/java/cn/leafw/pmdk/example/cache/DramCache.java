package cn.leafw.pmdk.example.cache;

/**
 * dram缓存
 *
 * @author <a href="mailto:wyr95626@95626.cn">CareyWYR</a>
 * @date 2022/7/7
 */
public class DramCache extends CustomCache{

    public DramCache(long baseAddress, int capacity) {
        super(baseAddress, capacity);
    }

    @Override
    public CustomCache slice() {
        int pos = this.position();
        int lim = this.limit();
        assert (pos <= lim);
        int rem = lim - pos;
        int off = (pos << 0);
        assert (off >= 0);
        return new DramCache(this.address() + off, rem);
    }


}

