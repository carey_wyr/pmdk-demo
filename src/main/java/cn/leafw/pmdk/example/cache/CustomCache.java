package cn.leafw.pmdk.example.cache;

import sun.misc.Unsafe;
import sun.nio.ch.DirectBuffer;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

/**
 * 自定义cache，或者说重写bytebuffer
 *
 * @author <a href="mailto:wyr95626@95626.cn">CareyWYR</a>
 * @date 2022/7/5
 */
public abstract class CustomCache {

    static Unsafe unsafe= ResourceManager.getUnsafe();

    private int capacity;

    private int limit;

    private int position;

    private long baseAddress;

    public CustomCache(long baseAddress, int capacity) {
        this.baseAddress = baseAddress;
        this.capacity = capacity;
        this.limit = capacity;
        this.position = 0;
    }


    // ------------------------------读写--------------------------------------


    /**
     * 写byte
     * @param x
     * @return CustomCache
     */
    public CustomCache put(byte x) {
        unsafe.putByte(ix(nextPutIndex()), ((x)));
        return this;
    }

    public CustomCache put(ByteBuffer src) {
        int n = src.remaining();
        if (n > remaining()) {
            throw new BufferOverflowException();
        }
        for (int i = 0; i < n; i++) {
            put(src.get());
        }
        return this;
    }

    public CustomCache put(byte[] src, int offset, int length) {
        checkBounds(offset, length, src.length);
        if (length > remaining()) {
            throw new BufferOverflowException();
        }
        int end = offset + length;
        for (int i = offset; i < end; i++) {
            this.put(src[i]);
        }
        return this;
    }

    public final CustomCache put(byte[] src) {
        return put(src, 0, src.length);
    }

    public byte get() {
        return ((unsafe.getByte(ix(nextGetIndex()))));
    }

    /**
     * 根据position获取byte
     * @param i position
     * @return byte
     */
    public byte get(int i) {
        return ((unsafe.getByte(ix(checkIndex(i)))));
    }


    // ------------------------------基础属性--------------------------------------

    public final int position() {
        return position;
    }


    public final CustomCache position(int newPosition) {
        if ((newPosition > limit) || (newPosition < 0)) {
            throw new IllegalArgumentException();
        }
        position = newPosition;
//        if (mark > position) mark = -1;
        return this;
    }

    public final int limit() {
        return limit;
    }

    public final int capacity() {
        return capacity;
    }

    public final CustomCache limit(int newLimit) {
        if ((newLimit > capacity) || (newLimit < 0)) {
            throw new IllegalArgumentException();
        }
        limit = newLimit;
        if (position > limit) {
            position = limit;
        }
//        if (mark > limit) {
//            mark = -1;
//        }
        return this;
    }

    /**
     * Returns the number of elements between the current position and the
     * limit.
     * 写数据之前一定要判断下remaining是否足够
     *
     * @return The number of elements remaining in this buffer
     */
    public final int remaining() {
        return limit - position;
    }


    public final long address() {
        return this.baseAddress;
    }

    /**
     * 切片
     * @return CustomCache
     */
    public abstract CustomCache slice();

    public ByteBuffer clipBuffer(int position, int size) {
        long start = ix(position);
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(size);
        DirectBuffer buffer = (DirectBuffer) byteBuffer;
        long address = buffer.address();
        unsafe.copyMemory(start, address, size);
        return byteBuffer;

    }


    // ------------------------------protected--------------------------------------


    protected int nextGetIndex() {
        if (position >= limit) {
            throw new BufferUnderflowException();
        }
        return position++;
    }

    protected int nextPutIndex() {
        if (position >= limit) {
            throw new BufferOverflowException();
        }
        return position++;
    }

    protected int checkIndex(int i) {
        if ((i < 0) || (i >= limit)) {
            throw new IndexOutOfBoundsException();
        }
        return i;
    }

    protected long ix(int i) {
        return baseAddress + ((long) i);
    }

    protected void checkBounds(int off, int len, int size) {
        if ((off | len | (off + len) | (size - (off + len))) < 0) {
            throw new IndexOutOfBoundsException();
        }
    }

}

