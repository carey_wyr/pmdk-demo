package cn.leafw.pmdk.example.cache;


/**
 * 先不用吧
 *
 * @author <a href="mailto:wyr95626@95626.cn">CareyWYR</a>
 * @date 2022/7/6
 */
public class MmapCache extends CustomCache{

    public MmapCache(long baseAddress, int capacity) {
        super(baseAddress, capacity);
    }

    @Override
    public CustomCache slice() {
        int pos = this.position();
        int lim = this.limit();
        assert (pos <= lim);
        int rem = lim - pos;
        int off = (pos);
        assert (off >= 0);
        return new MmapCache(this.address() + off, rem);
    }

}

