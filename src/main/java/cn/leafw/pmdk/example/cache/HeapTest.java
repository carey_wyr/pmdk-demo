package cn.leafw.pmdk.example.cache;

import sun.nio.ch.DirectBuffer;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;

import static java.nio.file.StandardOpenOption.*;

/**
 * @author carey
 */
public class HeapTest {

    public static void main(String[] args) {
        try {
            clipTest();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void mmapTest() throws Exception{
        String path = "/Users/carey/Documents/workspace/temp";
        FileChannel fileChannel=FileChannel.open(new File(path+File.separator+"mmap.data").toPath(),WRITE,CREATE,READ);
        MappedByteBuffer cacheMmap = fileChannel.map(FileChannel.MapMode.PRIVATE,0,1024*1024*1024L);
        long address = ((DirectBuffer)cacheMmap).address();
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(16);
        MmapCache mmapCache = new MmapCache(address, 16);
        System.out.println(mmapCache.remaining());
        mmapCache.put((byte) 'a');
        System.out.println(mmapCache.remaining());
        byte b = mmapCache.get(0);
        System.out.println(Byte.toString(b));
    }

    public static void bufferTest() throws IOException {
        String path = "/Users/carey/Documents/workspace/temp/buffer.txt";
        FileChannel fileChannel = new RandomAccessFile(new File(path), "rw").getChannel();
        ByteBuffer mappedByteBuffer = fileChannel.map(FileChannel.MapMode.READ_WRITE, 0, 4);
        ByteBuffer buffer = mappedByteBuffer.slice();
        buffer.position(0);
        buffer.put("a".getBytes(StandardCharsets.UTF_8));
        System.out.printf("position: {%d}, limit: {%d}, capacity: {%d}%n", buffer.position(), buffer.limit(), buffer.capacity());
        System.out.printf("position: {%d}, limit: {%d}, capacity: {%d}, get: {%s}%n", mappedByteBuffer.position(), mappedByteBuffer.limit(), mappedByteBuffer.capacity(), mappedByteBuffer.get());
    }

    public static void sliceTest() throws IOException {
        String path = "/Users/carey/Documents/workspace/temp/slice.txt";
        FileChannel fileChannel = new RandomAccessFile(new File(path), "rw").getChannel();
        MappedByteBuffer cacheMmap = fileChannel.map(FileChannel.MapMode.READ_WRITE,0,16);
        long address = ((DirectBuffer)cacheMmap).address();
        MmapCache mmapCache = new MmapCache(address, 16);
        CustomCache slice = mmapCache.slice();
        slice.position(0);
        slice.put((byte) 'a');
        System.out.printf("position: {%d}, limit: {%d}, capacity: {%d}, get: {%s}%n", mmapCache.position(), mmapCache.limit(), mmapCache.capacity(), mmapCache.get(1));
        System.out.printf("position: {%d}, limit: {%d}, capacity: {%d}, get: {%s}%n", slice.position(), slice.limit(), slice.capacity(), slice.get(0));
    }

    public static void clipTest() {
        DirectBuffer byteBuffer = (DirectBuffer) ByteBuffer.allocateDirect(16);
        long address = byteBuffer.address();
        DramCache dramCache = new DramCache(address, 16);
        dramCache.put("abcd".getBytes(StandardCharsets.UTF_8));
        System.out.println(dramCache.get(0));
        System.out.println(dramCache.get(1));
        ByteBuffer buffer = dramCache.clipBuffer(1, 2);
        System.out.println(buffer.get(0));
        System.out.println(buffer.get(1));


    }


}
