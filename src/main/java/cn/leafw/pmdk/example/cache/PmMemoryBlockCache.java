package cn.leafw.pmdk.example.cache;

/**
 * PmMemoryBlockCache
 *
 * @author <a href="mailto:wyr95626@95626.cn">CareyWYR</a>
 * @date 2022/7/4
 */
public class PmMemoryBlockCache extends CustomCache {

    public PmMemoryBlockCache(long baseAddress, int capacity) {
        super(baseAddress, capacity);
    }

    @Override
    public CustomCache slice() {
        int pos = this.position();
        int lim = this.limit();
        assert (pos <= lim);
        int rem = lim - pos;
        int off = (pos << 0);
        assert (off >= 0);
        return new PmMemoryBlockCache(this.address() + off, rem);
    }

}

