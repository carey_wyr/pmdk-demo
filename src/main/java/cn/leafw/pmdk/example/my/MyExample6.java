package cn.leafw.pmdk.example.my;

import com.intel.pmem.llpl.Heap;
import com.intel.pmem.llpl.MemoryBlock;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * TODO
 *
 * @author <a href="mailto:wyr95626@95626.cn">CareyWYR</a>
 * @date 2022/6/7
 */
public class MyExample6 {

    static {
        System.loadLibrary("mybuffer");
    }

    public native ByteBuffer JBuffer();

    public static void main(String[] args) {
        MyExample6 example = new MyExample6();
        example.JBuffer();

        Heap heap = Heap.createHeap("", 1);
        MemoryBlock memoryBlock = heap.allocateMemoryBlock(1);
    }

}

