package cn.leafw.pmdk.example.my;

/**
 * TODO
 *
 * @author <a href="mailto:wyr95626@95626.cn">CareyWYR</a>
 * @date 2022/6/7
 */
public class JniDemo {

    static {
        // Load native library at runtime
        System.loadLibrary("JniDemo");
        // hello.dll (Windows) or libhello.so (Unixes)
    }

    // Declare a native method sayHello() that receives nothing and returns void
    private native void sayHello();

    // Test Driver
    public static void main(String[] args) {
        new JniDemo().sayHello();  // invoke the native method
    }

}

