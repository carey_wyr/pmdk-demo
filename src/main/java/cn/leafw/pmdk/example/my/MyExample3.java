package cn.leafw.pmdk.example.my;

import sun.misc.Unsafe;

import java.lang.reflect.Field;

/**
 * TODO
 *
 * @author <a href="mailto:wyr95626@95626.cn">CareyWYR</a>
 * @date 2022/4/21
 */
public class MyExample3 {

    private  Unsafe unsafe;

    private static Unsafe reflectGetUnsafe() {
        try {
            Field field = Unsafe.class.getDeclaredField("theUnsafe");
            field.setAccessible(true);
            return (Unsafe) field.get(null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public MyExample3() {
        this.unsafe = reflectGetUnsafe();
    }

    public static void main(String[] args) {
        MyExample3 example = new MyExample3();
        long address = example.unsafe.allocateMemory(1024 * 1024);
        System.out.println(address);
        example.unsafe.putInt(address, 11);
        System.out.println(example.unsafe.getInt(140194407858176L));

    }
}

