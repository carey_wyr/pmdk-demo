package cn.leafw.pmdk.example.my;

import cn.leafw.pmdk.example.constant.Constant;
import com.intel.pmem.llpl.MemoryPool;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Random;

/**
 * TODO
 *
 * @author <a href="mailto:wyr95626@95626.cn">CareyWYR</a>
 * @date 2022/3/3
 */
public class Read {

    public static void main(String[] args) {
        try {
            read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void read() throws IOException {
        ByteBuffer buffer = ByteBuffer.allocateDirect(4);
        buffer.put("a".getBytes(StandardCharsets.UTF_8));
        int lastCommittedPosition = buffer.position();
        ByteBuffer slice = buffer.slice();
        //上一次的提交指针作为position
        slice.position(0);
        //当前最大的写指针作为limit
        slice.limit(lastCommittedPosition);
        File file = new File("/Users/carey/Documents/paper/temp/test.txt");
        if (!file.exists()) {
            boolean newFile = file.createNewFile();
            if (!newFile) {
                System.out.println("error");
            }
        }
        RandomAccessFile rw = new RandomAccessFile(file, "rw");
        FileChannel channel = rw.getChannel();
        //把commitedPosition到wrotePosition的写入FileChannel中
        channel.position(0);
        channel.write(slice);
    }
}

