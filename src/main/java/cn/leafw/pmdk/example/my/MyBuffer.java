package cn.leafw.pmdk.example.my;

import org.openjdk.jol.vm.VM;
import sun.nio.ch.DirectBuffer;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;

/**
 * TODO
 *
 * @author <a href="mailto:wyr95626@95626.cn">CareyWYR</a>
 * @date 2022/6/8
 */
public class MyBuffer {

    static {
        System.loadLibrary("MyBuffer");
    }

    private native void getBuffer (long address, int capacity);

    public static void main(String[] args) {
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(4);
        final long address = ((DirectBuffer) byteBuffer).address();

        new MyBuffer().getBuffer(address, 4);
        System.out.println("origin address = " + address);
        System.out.println("java address = " + Long.toHexString(address));



//        long buffer = new MyBuffer().getBuffer(byteBuffer);
//        System.out.println(buffer);
//        ByteBuffer buffer = new MyBuffer().getBuffer();
//        if (null == buffer) {
//            System.out.println("---");
//            return;
//        }
//        buffer.put("hello".getBytes(StandardCharsets.UTF_8));
//        buffer.flip();
//        try {
//            RandomAccessFile file = new RandomAccessFile(new File("/Users/carey/Documents/paper/temp/buffer.txt"), "rw");
//            FileChannel channel = file.getChannel();
//            channel.write(buffer);
//            channel.force(false);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }
}

