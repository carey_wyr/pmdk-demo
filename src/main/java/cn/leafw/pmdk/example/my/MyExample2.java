package cn.leafw.pmdk.example.my;

import com.intel.pmem.llpl.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.function.Supplier;

/**
 * TODO
 *
 * @author <a href="mailto:wyr95626@95626.cn">CareyWYR</a>
 * @date 2022/4/7
 */
public class MyExample2 {
    public static final Integer GB = 1024 * 1024 * 1024;
    public static final Integer KB = 1024 ;
    public static final String CHANNEL_PATH = "/Users/carey/Documents/paper/temp/bb";
    public static final String MMAP_PATH = "/Users/carey/Documents/paper/temp/bb";
    // 文件夹
    public static final String HEAP_PATH = "/Users/carey/Documents/paper/temp/bb";
    public static final String POOL_PATH = "/Users/carey/Documents/paper/temp/bb";

//    public static final String MMAP_PATH = "/mnt/pmem0/weigong/mm1";
//    // 文件夹
//    public static final String HEAP_PATH = "/mnt/pmem0/weigong/mm2";
//    public static final String POOL_PATH = "/mnt/pmem0/weigong/mm3";

    /**
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        long start = System.currentTimeMillis();
        File file = new File(MMAP_PATH);
        FileChannel fileChannel = new RandomAccessFile(file, "rw").getChannel();
        MappedByteBuffer mappedByteBuffer = fileChannel.map(FileChannel.MapMode.READ_WRITE, 0, GB);
        for (int i = 0; i < 4 * KB; i++) {
            mappedByteBuffer.put((byte) 0);
        }
        for (int i = 0; i < GB; i += 4 * KB) {
            mappedByteBuffer.position(0);
            mappedByteBuffer.limit(4 * KB);
            fileChannel.write(mappedByteBuffer);
        }
        long time1 = System.currentTimeMillis();
        System.out.println("文件映射耗时: " + (time1 - start));
        mappedByteBuffer.force();
        long time2 = System.currentTimeMillis();
        System.out.printf("刷盘耗时: %s, 总耗时: %s%n", time2 - time1, time2 - start);

//        mmapSingleByteWrite();
//        fileChannelWrite();
//        heapWrite();
//        poolWrite();

    }

    public static void mmapSingleByteWrite() {
        long start = System.currentTimeMillis();
        try {
            File file = new File(CHANNEL_PATH);
            FileChannel fileChannel = new RandomAccessFile(file, "rw").getChannel();
            MappedByteBuffer map = fileChannel.map(FileChannel.MapMode.READ_WRITE, 0, GB);
            for (int i = 0; i < GB; i++) {
                map.put((byte)0);
            }
            System.out.printf("总耗时: %s%n",System.currentTimeMillis()- start);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void fileChannelWrite() {
        long start = System.currentTimeMillis();
        File file = new File(CHANNEL_PATH);
        try {
            FileChannel fileChannel = new RandomAccessFile(file, "rw").getChannel();
            ByteBuffer byteBuffer = ByteBuffer.allocateDirect(4*KB);
            long time1 = System.currentTimeMillis();
            System.out.println("allocateDirect耗时: " + (time1 - start));
            for (int i = 0; i < 4*KB; i++) {
                byteBuffer.put((byte)0);
            }
            for (int i = 0; i < GB; i += 4*KB) {
                byteBuffer.position(0);
                byteBuffer.limit(4*KB);
                fileChannel.write(byteBuffer);
            }
            long time2 = System.currentTimeMillis();
            System.out.printf("write耗时: %s, 总耗时: %s%n", time2 - time1, time2 - start);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void heapWrite() {
        long start = System.currentTimeMillis();
        Heap heap = Heap.createHeap(HEAP_PATH);
        MemoryBlock block = heap.allocateMemoryBlock(GB);
        long time1 = System.currentTimeMillis();
        System.out.printf("分配堆和内存耗时: %s%n", time1 - start);
        for (int i = 0; i < GB; i++) {
            block.setInt(i, i);
        }
        long time2 = System.currentTimeMillis();
        System.out.printf("blockSetInt耗时: %s%n", time2 - time1);
        block.flush();
        long time3 = System.currentTimeMillis();
        System.out.printf("block刷盘耗时: %s, 总耗时: %s%n", time3 - time2, time3 - start);
    }

    public static void poolWrite() {
        long start = System.currentTimeMillis();
        MemoryPool pool = MemoryPool.createPool(POOL_PATH, GB + 4);
        long time1 = System.currentTimeMillis();
        System.out.printf("分配pool耗时: %s%n", time1 - start);
        for (int i = 0; i < GB; i++) {
            pool.setInt(i, i);
        }
        long time2 = System.currentTimeMillis();
        System.out.printf("set耗时: %s%n", time2 - time1);
        pool.flush(0, GB);
        long time3 = System.currentTimeMillis();
        System.out.printf("flush耗时: %s, 总耗时%s%n", time3 - time2, time3 - start);
    }

}
