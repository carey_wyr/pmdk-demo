package cn.leafw.pmdk.example.my;

import cn.leafw.pmdk.example.constant.Constant;
import com.intel.pmem.llpl.MemoryPool;

import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * TODO
 *
 * @author <a href="mailto:wyr95626@95626.cn">CareyWYR</a>
 * @date 2022/4/21
 */
public class MyExample4 {

    public static void main(String[] args) throws Exception{
        write("/Users/carey/Documents/paper/temp/ch");
        writePadding("/Users/carey/Documents/paper/temp/ch2");
        directWrite("/Users/carey/Documents/paper/temp/ch3");
    }

    public static void write(String path) throws Exception {
        RandomAccessFile file = new RandomAccessFile(path, "rw");
        FileChannel channel = file.getChannel();
        byte[] bytes = new byte[Constant.GB.intValue()];
        long time1 = System.currentTimeMillis();
        ByteBuffer byteBuffer = ByteBuffer.allocate(Constant.GB.intValue());
        byteBuffer.put(bytes);
        byteBuffer.flip();
        channel.write(byteBuffer);
//        channel.force(false);
        System.out.println(System.currentTimeMillis() - time1);
    }

    public static void writePadding(String path) throws Exception {
        RandomAccessFile file = new RandomAccessFile(path, "rw");
        FileChannel channel = file.getChannel();
        byte[] bytes = new byte[Constant.GB.intValue()];
        long time1 = System.currentTimeMillis();
        ByteBuffer byteBuffer = ByteBuffer.allocate(Constant.GB.intValue());
        for (int i = 0; i < 4 * Constant.KB; i++) {
//            int i1 = i % 2;
            byteBuffer.put((byte) 0);
        }
        for (int i = 0; i < bytes.length; i += 4 * Constant.KB) {
            byteBuffer.position(0);
            byteBuffer.limit(4 * Constant.KB.intValue());
            channel.write(byteBuffer);
        }
//        channel.force(true);
        System.out.println(System.currentTimeMillis() - time1);
    }

    public static void directWrite(String path) throws Exception{
        long time1 = System.currentTimeMillis();
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(Constant.GB.intValue());
        byte[] bytes = new byte[Constant.GB.intValue()];
        byteBuffer.put(bytes);
        byteBuffer.flip();
        RandomAccessFile file = new RandomAccessFile(path, "rw");
        FileChannel channel = file.getChannel();
        channel.write(byteBuffer);
//        channel.force(false);
        System.out.println(System.currentTimeMillis() - time1);

    }
}


