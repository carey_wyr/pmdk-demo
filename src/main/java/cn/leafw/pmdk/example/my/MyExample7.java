package cn.leafw.pmdk.example.my;

import com.intel.pmem.llpl.MemoryPool;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;

/**
 * TODO
 *
 * @author <a href="mailto:wyr95626@95626.cn">CareyWYR</a>
 * @date 2022/6/23
 */
public class MyExample7 {

    public static void main(String[] args) throws Exception{
        String path = "";
        FileChannel fileChannel = new RandomAccessFile(new File(path), "rw").getChannel();
        MemoryPool pool = MemoryPool.createPool(path, 4);
        ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        byteBuffer.put("abcd".getBytes(StandardCharsets.UTF_8));
        pool.copyFromByteArrayNT(byteBuffer.array(), byteBuffer.arrayOffset() + byteBuffer.position(), 0, 4);
        byte a = pool.getByte(0);
        System.out.println(a);

    }
}

